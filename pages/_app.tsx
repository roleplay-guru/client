import { AppTheme } from '@common/components/AppTheme';
import type { AppProps } from 'next/app';

function MyApp({ Component, pageProps }: AppProps) {
    return (
        <AppTheme>
            <Component {...pageProps} />
        </AppTheme>
    );
}

export default MyApp;
