export const Button = {
    baseStyle: {
        transition: 'background-color .25s, transform .25s, box-shadow .25s',
        borderRadius: 'md',

        _active: {
            transform: 'scale(0.95)',
        },
    },
    variants: {
        solid: ({ colorScheme }) => ({
            backgroundColor: `${colorScheme}.500`,
            color: `${colorScheme}.text`,
            boxShadow: `button.${colorScheme}.base`,

            _focus: {
                boxShadow: `button.${colorScheme}.focus`,
            },

            _hover: {
                backgroundColor: `${colorScheme}.600`,
                boxShadow: `button.${colorScheme}.focus`,
            },

            _active: {
                backgroundColor: `${colorScheme}.700`,
                boxShadow: `button.${colorScheme}.pressed`,
            },
        }),
        link: ({ colorScheme }) => ({
            display: 'inline',
            fontSize: 'inherit',
            fontWeight: 'inherit',
            verticalAlign: 'text-top',
            px: 0,
            py: 0,
            color: `${colorScheme}.300`,
            transform: 'none !important',

            _hover: {
                color: `${colorScheme}.200`,
                textDecoration: 'underline',
            },

            _active: {
                color: `${colorScheme}.50`,
            },
        }),
    },
    sizes: {
        tiny: {
            px: 2,
            py: 2,
            fontSize: 'xs',
            fontWeight: 'medium',
        },
        small: {
            px: 2,
            py: 2,
            fontSize: 'sm',
            fontWeight: 'medium',
        },
        normal: {
            px: 4,
            py: 2,
            fontWeight: 'medium',
        },
        large: {
            px: 6,
            py: 4,
            fontSize: 'lg',
            fontWeight: 'medium',
        },
        huge: {
            px: 10,
            py: 6,
            fontSize: 'xl',
            fontWeight: 'medium',
        },
    },
    defaultProps: {
        variant: 'solid',
        size: 'normal',
    },
};
