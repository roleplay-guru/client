export const Text = {
    baseStyle: {
        color: 'text.body',
        fontSize: 'normal',
        lineHeight: 'base',
    },
};
