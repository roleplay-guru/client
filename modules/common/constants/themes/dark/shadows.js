import colors from './colors';

const variants = ['primary', 'secondary', 'positive', 'negative', 'neutral'];

const shadows = {
    button: {
        ...variants.reduce((temp, variant) => ({
            ...temp,
            [variant]: {
                base: `0 2px 8px 2px ${colors[variant].shadow}`,
                pressed: `0 1px 6px 2px ${colors[variant].shadow}`,
                focus: `0 0 12px 4px ${colors[variant].shadow}`,
            },
        }), {}),
    },
};

export default shadows;
