import colors from './colors';
import { extendTheme, withDefaultColorScheme } from '@chakra-ui/react';
import shadows from './shadows';
import { globalStyles } from './globalStyles';
import * as overrides from '../overrides';

export const dark = extendTheme({
    colors,
    shadows,
    components: overrides,
    styles: {
        global: globalStyles,
    },
}, withDefaultColorScheme({
    colorScheme: 'primary',
}));
