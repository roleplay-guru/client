import Color from 'color';

function colorScheme(color, step = 0.1) {
    return {
        0: color.lighten(5 * step).hex(),
        50: color.lighten(4.5 * step).hex(),
        100: color.lighten(4 * step).hex(),
        200: color.lighten(3 * step).hex(),
        300: color.lighten(2 * step).hex(),
        400: color.lighten(step).hex(),
        500: color.hex(),
        600: color.darken(step).hex(),
        700: color.darken(2 * step).hex(),
        800: color.darken(3 * step).hex(),
        900: color.darken(4 * step).hex(),
        shadow: color.lighten(step).alpha(0.45).hexa(),
    };
}

const surface = Color('#1b1e38');
const primary = Color('#6a66a3');
const secondary = Color('#ea5e3f');
const success = Color('#0ead69');
const error = Color('#ee4266');
const warning = Color('#ffd23f');

const colors = {
    primary: {
        ...colorScheme(primary),
        text: '#fff',
    },

    secondary: {
        ...colorScheme(secondary),
        text: '#fff',
    },

    positive: {
        ...colorScheme(success),
        text: '#fff',
    },

    negative: {
        ...colorScheme(error),
        text: '#fff',
    },

    neutral: {
        ...colorScheme(warning),
        text: 'black',
    },

    text: {
        body: '#fff',
    },

    surface: {
        0: surface.hex(),
        50: surface.lighten(0.25).hex(),
        100: surface.lighten(0.5).hex(),
        200: surface.lighten(1).hex(),
        300: surface.lighten(1.5).hex(),
        400: surface.lighten(2).hex(),
        500: surface.lighten(2.5).hex(),
        600: surface.lighten(3).hex(),
        700: surface.lighten(3.5).hex(),
        800: surface.lighten(4).hex(),
        900: surface.lighten(4.5).hex(),
    },
};

export default colors;
