export const globalStyles = {
    'html, body': {
        backgroundColor: 'surface.0',
        color: 'text.body',
    },
    'button, a': {
        outline: 'none',

        _focus: {
            boxShadow: 'none',
            outline: 'none',
        },
    },
};
