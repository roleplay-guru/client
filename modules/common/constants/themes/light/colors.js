import Color from 'color';

const surface = Color('#1b1e38');
const primary = Color('#ea5e3f');
const secondary = Color('#4fc1e8');
const positive = Color('#a0d568');
const negative = Color('#ed5564');
const neutral = Color('#ffce54');
