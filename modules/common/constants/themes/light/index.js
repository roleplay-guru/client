import { extendTheme } from '@chakra-ui/react';
import colors from '../dark/colors';

export const light = extendTheme({
    colors,
});
