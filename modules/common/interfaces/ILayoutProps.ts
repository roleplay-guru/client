import { ReactNode } from "react";

type ILayoutProps = {
    title?: string,
    children?: ReactNode,
}

export default ILayoutProps;
