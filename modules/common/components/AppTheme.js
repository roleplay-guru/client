import * as themes from '../constants/themes';
import { useMemo, useState } from 'react';
import { ThemeContext } from '../utils/context';
import { ChakraProvider } from '@chakra-ui/react';

export const AppTheme = ({ children }) => {
    const [themeName, setTheme] = useState('dark');

    const theme = themes[themeName] || themes.dark;
    
    const context = useMemo(() => ({
        currentTheme: themeName,
        changeTheme: setTheme,
    }), [themeName, setTheme]);

    return (
        <>
            <ThemeContext.Provider value={context}>
                <ChakraProvider theme={theme}>
                    {children}
                </ChakraProvider>
            </ThemeContext.Provider>
        </>
    );
};
