import Head from 'next/head';
import ILayoutProps from '@common/interfaces/ILayoutProps';
import React from 'react';
import { Box } from '@chakra-ui/react';

export const Brand : React.FC<ILayoutProps> = ({
    title,
    children,
}) => {
    return (
        <>
            <Head>
                <title>{title || 'Roleplay Guru'}</title>
            </Head>
            <Box>
                {children}
            </Box>
        </>
    );
};
